◎ 片名: 星球大战9：天行者崛起 Star Wars: The Rise of Skywalker

◎ 导演: J·J·艾布拉姆斯

◎ 编剧: J·J·艾布拉姆斯 / 克里斯·特里奥 / 乔治·卢卡斯 / 科林·特雷沃罗 / 德里克·康纳利

◎ 类型: 动作 / 科幻 / 冒险

◎ 制片国家/地区: 美国

◎ 语言: 英语

◎ 上映日期: 2019-12-18(中国大陆) / 2019-12-20(美国)

◎ 片长: 142分钟

◎ 又名: 星球大战9 / 星战9 / Star Wars: Episode IX / Star Wars 9

◎ IMDB 链接： https://www.imdb.com/title/tt2527338

◎ 剧情: 
本片为2017年电影《星球大战：最后的绝地武士》的续集，“星球大战后传三部曲”的第三部作品，同时是“天行者传奇”的第九部作品以及最终章。在卢克·天行者化为绝地英灵的一年后，残存的抵抗势力将再次面对第一秩序。同时，绝地和西斯之间的大战将天行者的传奇带到最后。
 

◎ 豆瓣链接 (6.5): https://movie.douban.com/subject/22265687/

================ 资源 ================ 
## Google Drive 9.9G

https://drive.google.com/drive/folders/1EhizVYQJRVWcqEp1hxL_I9vk7CskRLi9



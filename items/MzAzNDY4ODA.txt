◎ 片名: 银行家 The Banker

◎ 导演: 乔治·诺非

◎ 编剧: 布拉德·凯恩 / Niceole R. Levy / 乔治·诺非 / David Lewis Smith / Stan Younger

◎ 类型: 剧情

◎ 制片国家/地区: 美国

◎ 语言: 英语

◎ 上映日期: 2020-03-06(美国)

◎ 片长: 120分钟

◎ IMDB 链接： https://www.imdb.com/title/tt6285944

◎ 剧情: 
乔治·诺非将执导全新影片[银行家](The Banker，暂译)，塞缪尔·杰克逊、安东尼·麦凯、尼古拉斯·霍尔特、尼娅·朗等主演。乔治·诺非、尼科·莱维共同操刀剧本。Romulus Entertainment融资。影片基于伯纳德·加勒特(安东尼·麦凯饰)和乔·莫里斯(塞缪尔·杰克逊饰)两个非裔美国企业家的真实故事改编，1950年代他们试图挣脱种族歧视的传统枷锁，招募白人工人为其工作。尼古拉斯·霍尔特扮演他们商业帝国的一名白人负责人。目前影片在亚特兰大展开拍摄。
 

◎ 豆瓣链接 (7.6): https://douban.com/subject/30346880/

================ 资源 ================ 
## 1080p 中英字幕

magnet:?xt=urn:btih:B5F645C27B6CC8C4200F9E02529FE68737FD3BFC

https://pan.baidu.com/s/1P4NPFQwcYKGDoPy2qcIsgA
78mq 


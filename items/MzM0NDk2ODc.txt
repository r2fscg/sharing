◎ 片名: 绝境希望 Håp

◎ 导演: Maria Sødahl

◎ 编剧: Maria Sødahl

◎ 类型: 剧情

◎ 制片国家/地区: 挪威 / 瑞典

◎ 语言: 挪威语 / 瑞典语

◎ 上映日期: 2019-11-22(挪威)

◎ 片长: 125分钟

◎ 又名: Hope

◎ IMDB 链接： https://www.imdb.com/title/tt9812614

◎ 剧情: 
A couple with a large blended family has grown apart. When the wife is diagnosed with terminal brain cancer, their life breaks down and exposes neglected love.
 

◎ 豆瓣链接 (): https://movie.douban.com/subject/33449687/

================ 资源 ================ 

## 1080p 中挪字幕 mp4 file 

OneDrive https://bit.ly/2UkGaTx

◎ 片名: 9号秘事 第五季 Inside No. 9 Season 5

◎ 导演: 马特·利普西 / 吉列尔莫·莫拉莱斯 / 史蒂夫·佩姆伯顿

◎ 编剧: 里斯·谢尔史密斯 / 史蒂夫·佩姆伯顿

◎ 类型: 喜剧 / 悬疑 / 惊悚

◎ 制片国家/地区: 英国

◎ 语言: 英语

◎ 首播: 2020-02-04(中国大陆) / 2020-02-03(英国)

◎ 集数: 6

◎ 单集片长: 29分钟

◎ IMDB 链接： https://www.imdb.com/title/tt9118526

◎ 剧情: 
《9号秘事》是一部英式黑色幽默悬疑喜剧。一部只有短短的几集，每一集都讲一个独立的故事。每一集只有短短的半个小时，却以十分紧凑的剧情和片尾的意外结局让观众大为赞赏。
 

◎ 豆瓣链接 (8.6): https://douban.com/subject/30142227/

================ 资源 ================ 

## Season 5 720p 1 ~ 6 
magnet:?xt=urn:btih:dacbcdc3444003111fbcf9b7cf51bcdd44d21127 
magnet:?xt=urn:btih:af4a13e634a842b7eb99fbbd4c340beeea15cdc0 
magnet:?xt=urn:btih:5cfda1edbd022c904b949152b61514f0c01fa458 
magnet:?xt=urn:btih:57b62058f481462c665cd99ceb0d6fea37bbfe63 
magnet:?xt=urn:btih:77A7349608B4C4C3BD9ED4F7D9B18AB0CCBF49CF 
magnet:?xt=urn:btih:0D7AF98A05A11871600C4CC9212F9592DE538148 



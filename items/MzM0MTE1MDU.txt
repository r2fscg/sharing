◎ 片名: 从不，很少，有时，总是 Never Rarely Sometimes Always

◎ 导演: 伊丽莎·希特曼

◎ 编剧: 伊丽莎·希特曼

◎ 类型: 剧情

◎ 官方网站: www.focusfeatures.com/never-rarely-sometimes-always

◎ 制片国家/地区: 美国

◎ 语言: 英语

◎ 上映日期: 2020-01-24(圣丹斯电影节) / 2020-02-25(柏林电影节) / 2020-03-13(美国)

◎ 片长: 101分钟

◎ 又名: 从不、很少、有时、总是 / 绝不、很少、有时、总是

◎ IMDB 链接： https://www.imdb.com/title/tt7772582

◎ 剧情: 
电影刻画了美国宾夕法尼亚州农村两个少女的故事。在意外怀孕后，Autumn和她的表亲Skylar在家乡遭到了不友好的对待，于是两人启程前往纽约，进行了一场跨越州际之旅。
 

◎ 豆瓣链接 (7.6): https://douban.com/subject/33411505/

================ 资源 ================ 
1080p pan.baidu 
https://pan.baidu.com/share/init?surl=c2ENEdW_0SMdg6RZBqyUCA
o9ho
